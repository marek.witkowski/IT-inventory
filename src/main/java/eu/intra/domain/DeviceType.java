package eu.intra.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "devicetypes")
public class DeviceType {

    @Id
    @GeneratedValue
    private double id;
    private String deviceTypeName;

    public DeviceType() {
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getDeviceTypeName() {
        return deviceTypeName;
    }

    public void setDeviceTypeName(String deviceTypeName) {
        this.deviceTypeName = deviceTypeName;
    }


    @Override
    public String toString() {
        return deviceTypeName;
    }
}

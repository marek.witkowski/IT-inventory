package eu.intra.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "history")
public class HistoryOfDevices {


    @Id
    @GeneratedValue
    BigDecimal id;
    LocalDate dateOfevent;
    Double deviceId;
    Double eventType;
    Double userId;
    String desctiption;


    HistoryOfDevices() {
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public LocalDate getDateOfevent() {
        return dateOfevent;
    }

    public void setDateOfevent(LocalDate dateOfevent) {
        this.dateOfevent = dateOfevent;
    }

    public Double getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Double deviceId) {
        this.deviceId = deviceId;
    }

    public Double getEventType() {
        return eventType;
    }

    public void setEventType(Double eventType) {
        this.eventType = eventType;
    }

    public Double getUserId() {
        return userId;
    }

    public void setUserId(Double userId) {
        this.userId = userId;
    }

    public String getDesctiption() {
        return desctiption;
    }

    public void setDesctiption(String desctiption) {
        this.desctiption = desctiption;
    }



}

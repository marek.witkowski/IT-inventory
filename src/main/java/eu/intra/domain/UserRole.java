package eu.intra.domain;

public enum UserRole {

    ADMINISTRATOR,
    NORMAL_USER,
    SYSTEM_USER

}
